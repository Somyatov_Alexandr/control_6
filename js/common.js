$(function() {

  var userData = {};
  var userPosts = {};
  var usersEmail = {};
  var userEmail = 'alex.som86@gmail.com';
  var baseUrl = "http://146.185.154.90:8000/blog/" + userEmail + '/profile';
  var postsUrl = "http://146.185.154.90:8000/blog/" + userEmail + "/posts";
  var subscribeUrl = "http://146.185.154.90:8000/blog/" + userEmail + "/subscribe";

  function getProfileHtml(response) {
    $(".profile__name").html(response.firstName + ' ' + response.lastName);
  }

  function getMessageItemToHtml(response) {
    var messageItem = $('<div class="message__item">');
    var author = $('<div class="message__author">').html(response.user.firstName + " " + response.user.lastName + " said: ");
    var message = $('<div class="message__body">').html(response.message);
    messageItem.append(author, message);
    $(".message").prepend(messageItem);
  }

  function getPostsToHtml(response) {
    for (let i = 0; i < response.length; i++) {
      var element = response[i];
      getMessageItemToHtml(element);
    }
  }

  $(".user-edit").on("submit", function() {
    userData.firstName = $("#firstName").val();
    userData.lastName = $("#lastName").val();

    editProfile();

    $("#send-profile").fadeOut();
  });

  $("#add-message").on("submit", function() {
    userPosts.message = $("#messageText").val();

    sendPosts();
    $(this).trigger('reset');
  });

  $('#send-email').on('submit', function() {
    usersEmail.email = $("#email").val();

    if (usersEmail.email) {
      makeSubscribe();
    }

    getPosts();
    usersEmail.email = undefined;
  })

  

  function getProfile() {
    $.ajax({
      type: 'GET',
      url: baseUrl
    }).then(function(response) {
      userData = response;
      getProfileHtml(response);
      // console.log(userData);
    });

  }

  function editProfile() {
    $.ajax({
      type: "POST",
      url: baseUrl,
      data: userData
    }).then(function(response) {
      userData = response;
      getProfileHtml(response);
    });
  }

  getProfile();

  function getPosts() {
    $.ajax({
      type: 'GET',
      url: postsUrl
    }).then(getPostsToHtml);
  }

  function sendPosts() {
    $.ajax({
      type: "POST",
      url: postsUrl,
      data: userPosts
    }).then(function() {
      $.ajax({
        type: 'GET',
        url: postsUrl,
        success: function(response) {
          var lastItem = response[response.length - 1];
          getMessageItemToHtml(lastItem);
        }
      })
    });
  }
  
  function makeSubscribe() {
    $.ajax({
      type: 'POST',
      url: subscribeUrl,
      data: usersEmail
    }).then(function(response) {
      if (response.error) {
        alert(response.error);
      }
    });
  }
  
  function getSubscribeList() {
    $.ajax({
      type: 'GET',
      url: subscribeUrl,
      success: function(resp) {
        console.log(resp);
      }
    });
  }
  
  getPosts();

  // setInterval(getPosts, 3000);
  // getSubscribeList();
  // sendPosts();
  
  $(".follow__link").on("click", function(e) {
    e.preventDefault();

    $("#send-email").fadeIn();
  });
  
  $('.edit__link').on('click', function(e) {
    e.preventDefault();
    
    $("#send-profile").fadeIn();
  });
  
  
  
  $(".subcribe").on("submit", function() {
    $("#send-email").fadeOut();
  });

  $('.close').on('click', function() {
    $(this).closest('.modal-wrap').fadeOut();
  });
  
});